#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

source $SCRIPT_DIR/common.sh 

install_arch_packages() {
  sudo pacman -Sy - < "$SCRIPT_DIR/../arch/packages"
}

install_aur_packages() {
  yay -Sy - < "$SCRIPT_DIR/../aur/packages"
}

install_yay() {
  [ ! -d ~/yay ] && git clone https://aur.archlinux.org/yay.git ~/yay
  cd ~/yay
  makepkg -si
  cd -
}

setup_arch() {
  install_arch_packages
  install_yay
  install_aur_packages
}

setup_arch
install_common
