#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

source $SCRIPT_DIR/common.sh 

KUBECTX_VERSION=v0.9.1

add_apt_keys() {
  grep -v '^ *#' <apt/keys | while IFS= read -r line; do
    curl -fsSL "$line" | sudo apt-key add -
  done
  grep -v '^ *#' <apt/keys-custom-keyring | while IFS= read -r line; do
    key=$(cut -d' ' -f1 <<<"$line")
    keyring=$(cut -d' ' -f2 <<<"$line")
    curl -s "$key" | sudo apt-key --keyring "${keyring}" add -
  done
}

add_apt_repos() {
  grep -v '^ *#' <apt/repositories | while IFS= read -r line; do
    sudo apt-add-repository --yes --update "${line}"
  done
}

update_sources_d() {
  for file in ./apt/sources.list.d/*; do
    basename=$(basename "${file}")
    cat "${file}" | sudo tee "/etc/apt/sources.list.d/${basename}"
  done
}

add_docker_repo() {
  sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
}

install_apt_packages() {
  sudo apt update
  sudo apt install -y $(grep -vE "^\s*#" apt/packages | tr "\n" " ")
}

install_deb_packages() {
  grep -v '^ *#' <deb/packages | while IFS= read -r line; do
    wget "${line}"
  done
  for file in *.deb; do
    sudo dpkg -i "${file}"
  done
  rm ./*.deb
}

install_snap_packages() {
  grep -v '^ *#' <snap/packages | while IFS= read -r line; do
    sudo snap install ${line}
  done
}

install_golang() {
  curl -sL https://golang.org/dl/go1.16.7.linux-amd64.tar.gz | sudo tar -xz -C /usr/local
}

install_flux() {
  curl -Ls https://toolkit.fluxcd.io/install.sh | sudo bash
}

install_goreleaser() {
  curl -sfL https://install.goreleaser.com/github.com/goreleaser/goreleaser.sh | sh
}

install_graalvm() {
  sudo mkdir -p /usr/lib/jvm
  curl -sL https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.1.0/graalvm-ce-java11-linux-amd64-20.1.0.tar.gz | \
    sudo tar xvz -C /usr/lib/jvm/
  sudo ln -s /usr/lib/jvm/graalvm-ce-java11-20.1.0 /usr/lib/jvm/graalvm
  sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/graalvm/bin/javac 1
  sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/graalvm/bin/java 1
}

install_k3d() {
  wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | sudo bash
}

install_k9s() {
  curl -sL  https://github.com/derailed/k9s/releases/download/v0.21.7/k9s_Linux_x86_64.tar.gz  | \
    sudo tar xvz -C /usr/local/bin && \
    sudo rm /usr/local/bin/README.md /usr/local/bin/LICENSE
}

install_kubectx_kubens() {
  TEMP_DIR=$(mktemp -d)
  curl -sL "https://github.com/ahmetb/kubectx/releases/download/${KUBECTX_VERSION}/kubectx_${KUBECTX_VERSION}_linux_x86_64.tar.gz" | tar -xz -C "${TEMP_DIR}"
  curl -sL "https://github.com/ahmetb/kubectx/releases/download/${KUBECTX_VERSION}/kubens_${KUBECTX_VERSION}_linux_x86_64.tar.gz" | tar -xz -C "${TEMP_DIR}"
  sudo mv "${TEMP_DIR}/kubectx" /usr/local/bin/
  sudo mv "${TEMP_DIR}/kubens" /usr/local/bin/
}

install_kubeseal() {
  wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.12.5/kubeseal-linux-amd64 -O kubeseal
  sudo install -m 755 kubeseal /usr/local/bin/kubeseal
  rm -rf ./kubeseal
}

install_kustomize() {
  curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
  sudo install -m 755 kustomize /usr/local/bin/kustomize
  rm -rf ./kustomize
}

install_jetbrains_toolbox() {
  curl -sL https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.21.9712.tar.gz  | \
    sudo tar xvz -C /opt
}

install_mkcert() {
  wget https://github.com/FiloSottile/mkcert/releases/download/v1.4.1/mkcert-v1.4.1-linux-amd64 -O mkcert
  sudo install -m 755 mkcert /usr/local/bin/mkcert
  rm -rf ./mkcert
}

install_popeye() {
  curl -sL  https://github.com/derailed/popeye/releases/download/v0.9.7/popeye_Linux_x86_64.tar.gz | \
    sudo tar xvz -C /usr/local/bin && \
    sudo rm /usr/local/bin/README.md /usr/local/bin/LICENSE
}

install_rke() {
  curl -sL https://github.com/rancher/rke/releases/download/v1.1.7/rke_linux-amd64 -O && \
    sudo mv rke_linux-amd64 /usr/local/bin/rke && \
    sudo chmod +x /usr/local/bin/rke
}

install_stern() {
  wget https://github.com/wercker/stern/releases/download/1.11.0/stern_linux_amd64 -O stern
  sudo install -m 755 stern /usr/local/bin/stern
  rm -rf ./stern
}

setup_ubuntu() {
#  add_apt_repos
#  add_apt_keys
#  update_sources_d
#  add_docker_repo
#  install_apt_packages
#  install_deb_packages
#  install_snap_packages
#  install_golang

#  install_flux
#  install_goreleaser
  install_graalvm
  install_k3d
  install_k9s
  install_kubectx_kubens
  install_kubeseal
  install_kustomize
  install_jetbrains_toolbox
  install_mkcert
  install_popeye
  install_rke
  install_stern
}

setup_ubuntu
install_common
