#!/usr/bin/env bash

install_sdkman() {
  curl -s "https://get.sdkman.io?rcupdate=false" | bash
}

setup_git() {
  cp .gitconfig ~/
}

install_nerd_fonts() {
  mkdir -p ~/.local/share/fonts
  curl -fLo ~/.local/share/fonts/"JetBrainsMono for Powerline Nerd Font Complete.otf" \
	https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/JetBrainsMono/Ligatures/Regular/complete/JetBrains%20Mono%20Regular%20Nerd%20Font%20Complete.ttf
}

prepare_docker() {
  sudo groupadd docker || echo "docker group already exists"
  sudo usermod -aG docker "$USER"
}

prepare_kvm() {
  sudo groupadd kvm || echo "group kvm already exists"
  sudo usermod -aG kvm "$USER"
}

install_gopass_kubeseal() {
  curl -sfL https://gitlab.com/FelixZ92/gopass-kubeseal/-/releases/v0.4.0/downloads/gopass-kubeseal_0.4.0_Linux_x86_64.tar.gz | \
    sudo tar xvz -C /usr/local/bin && \
    sudo rm /usr/local/bin/README.md /usr/local/bin/LICENSE
}

install_oh_my_zsh() {
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}


install_common () {
  install_sdkman
  setup_git
  install_nerd_fonts
  prepare_docker
  prepare_kvm
  install_gopass_kubeseal
  install_oh_my_zsh
}
