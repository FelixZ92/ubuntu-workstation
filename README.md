# Ubuntu 20.04 Workstation setup

A simple shell script (`setup.sh`) to install the following packages:

- [apt](apt/packages)

- [deb](deb/packages)

- [snap](snap/packages)

Aside from that, sdkman, nerdfonts and oh-my-zsh will be installed as well and git is [configured](.gitconfig).

### zsh setup:
``setup_zsh.sh`` installs the plugins `zsh-autosuggestions` `zsh-syntax-highlighting` and `powerlevel10k`

